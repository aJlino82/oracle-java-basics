package lino.choice;

public class ShopApp {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        Customer c1 = new Customer();
        c1.setName("Adailton Júnior");
        c1.setSize("M");

        //Instanciando entidades -> agora com sets e gets
        Clothing item1 = new Clothing("Blue Jacket", 29.9, "M");
        Clothing item2 = new Clothing("Orange T-Shirt", 9, "M");

        //Criando um array(vetor) de Clothing
        Clothing[] cloths = {item1, item2, new Clothing("Green T-Shirt", 5.0, "S"), new Clothing("Blue T-Shirt", 10.0, "XL")};

        int measurement = 9;

        c1.setSize(measurement);

        c1.addItems(cloths);

        System.out.println("::BEM VINDO AO SHOPP:: " + c1.getName() + " - " + c1.getSize());
        System.out.println("------------------------------------- \n");

        System.out.println("Preço mínimo: " + Clothing.MIN_PRICE);

        int avarage = 0;
        int count = 0;

        System.out.println("------------------------------------- \n");

        for (Clothing clo : c1.getItems()) {
            if (clo.getSize().equals("L")) {
                count++;
                avarage += clo.getPrice();
            }
        }
        
        try {
        avarage = (avarage / count);
        System.out.println("Avarage price: " + avarage + ", Count" + count);
        } catch (ArithmeticException e) {
            System.out.println("Não pode ser dividido por 0");
        }
        System.out.println("\n------------------------------------- \n");

    }

}
